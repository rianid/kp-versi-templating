create table penjual (
	username varchar(50) not null ,
	password varchar(50) not null ,
	constraint pk_penjual primary key (username) 
);
create table kategori(
	id_kategori int(10) not null auto_increment,
	nama_kategori varchar(100) not null,
	constraint pk_kategori primary key(id_kategori)
);
create table produk (
	username varchar(50) not null ,
	id_barang int(10) not null auto_increment,
	id_kategori int(10) not null,
	nama varchar(100) not null,
	jumlah int(10) not null,
	constraint pk_produk primary key (id_barang),
	constraint fk_produk1 foreign key (username) references penjual(username) on delete cascade,
	constraint fk_produk2 foreign key (id_kategori) references kategori(id_kategori) on delete cascade
);